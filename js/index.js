"use strict"


document.querySelector(".btn").addEventListener("click", (event) => {
	event.preventDefault();
	document.body.classList.toggle("light");
	if (document.body.matches(".light")) {
		localStorage.setItem("theme", "light");
	} else {
		localStorage.setItem("theme", "");
	}
});

let activeTheme = localStorage.getItem("theme");
if (activeTheme === "light") {
	document.body.classList.add("light");
}